<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'lyreco');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'lyreco');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'lyreco');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '17TKSXf8sQAfntObjNpWl.w`[K;b?5vJfBVFaynNV,*Onx*66q-zm}C<[^0Yr$4k');
define('SECURE_AUTH_KEY',  'I-<i0|Q0Sj3_{)_P4}C.TH&y#=eHZ2zA%]M-.yy~o,$J{?VE7J1[s[r!+vvd/6`~');
define('LOGGED_IN_KEY',    'zkubM`%b$+ MNFj?/Nh3U6Mb(cIB!k/kCtfU/55*lRF`k|M5TU;m)y#.w<NS@X;C');
define('NONCE_KEY',        'QL6r|A/qpw(cYIVcB8mUE1wI$4sjQU}u] GX[c3K7gMx1]xhkw@[HEfUS^xk$b5N');
define('AUTH_SALT',        's:JRz`6j>TgYVh|}H1Em;Mrhe#b?TD*/tre&SPKj47?tyW~Y~Aj.kZ<FT-^&l0G#');
define('SECURE_AUTH_SALT', 'J`louz+G/|j]MLi0ucg:[;OT(IrTZK`@#N/FME;5qu2GDOf>f::$Uy}hl)BBEl&{');
define('LOGGED_IN_SALT',   'pC_pZX?R HBVA*1ei]Y~DBp@[;&04a+zgDX~#NqRPwLin^K0]`u#]?T6>yB]sYh>');
define('NONCE_SALT',       'Mcu;~VNcbOKjIo#GeKaRRbiiL_d7H$yDts.%<Z2`YyVKDrT#[>Y,.-S1B3IgWi./');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
