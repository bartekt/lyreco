<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Ekspresy</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
    
    <body class="categoryEkspress">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
        
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>
                                Nieodłączny element<br />życia każdej firmy
                            </h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container">
            <h4>poznaj nasze ekspresy</h4>
        </div> 
        <div class="container"> 
            <div> 
                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemAugila">
                    
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Augila 420</h2>
                                </div>
                                <div class="col-md-7">
                                     <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                        <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="img/assets/augila420.png" alt="augila420">
                            <div class="labelTriangle">
                                <div> 
                                    <i></i>
                                    <p><small>od</small>100</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>

                </div>

                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemAugila">
                    
                    <div class="row">

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Augila 220</h2>
                                </div>
                                <div class="col-md-7">
                                     <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="img/assets/augila220.png" alt="augila220">
                            <div class="labelTriangle">
                                <div>
                                    <i></i>
                                    <p><small>od</small>50</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>

                </div>

                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemGemini">

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Gemini 500</h2>
                                </div>
                                <div class="col-md-7">
                                     <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 

                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="img/assets/gemini223.png" alt="gemini223">
                            <div class="labelTriangle">
                                <div>
                                    <i></i>
                                    <p><small>od</small>20</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>
                </div>

                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemSystemTower">

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>System Tower</h2>
                                </div>
                                <div class="col-md-7">
                                    <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 

                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="img/assets/system_tower1.png" alt="system_tower">
                            <div class="labelTriangle">
                                <div> 
                                    <i></i>
                                    <p><small>od</small>20</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-md-6 col-sm-12 col-xs-12 itemBox itemsZenius">

                    <div class="row"> 

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Zenius</h2>
                                </div>
                                <div class="col-md-7">
                                    <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 

                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="img/assets/zenusi.png" alt="zenusi">
                            <div class="labelTriangle">
                                <div>
                                    <i></i>
                                    <p><small>od</small>20<small>do</small>30</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>

                </div>

            </div> <!-- ITEM ROW END -->
        </div> <!-- CONTAINER END -->
    </section>

        
    <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


    </body>
</html> 
