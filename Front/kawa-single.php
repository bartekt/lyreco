<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Kawa</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
    
    <body class="single-kawa">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
                <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
    

    <section id="subPageContent">
        <div class="container">  
            <div class="row">  
			  <div class="single-box">
			  	<div class="col-md-6">
				  	<div class="product_img">
				  		<div><img src="img/assets/assets_item/espresso_legero.png" alt=""></div>
				  	</div>
			  	</div>
			  	<div class="col-md-6 ">
					<div class="description">
						<div class="tabs">
							<h1>Espresso Leggero</h1>
							<span>LEKKA I ORZEŹWIAJĄCA</span>
							 <!-- Nav tabs -->
							  <ul class="nav nav-tabs" role="tablist">
							    <li role="presentation" class="active">
                    
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Podgląd</a></li>
							    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Szczegóły</a></li>
							    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Składniki</a></li> 
							  </ul>

							  <!-- Tab panes -->
							  <div class="tab-content">
							    <div role="tabpanel" class="tab-pane active" id="home">
									<p>Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża 
									z doskonale zbilansowanym głównym bukietem</p>

                                    <ul>
                                        <li>
                                            <span>Intensywność:</span>
                                            <em>6</em>
                                            <ol>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                            </ol>
                                        </li>  
                                        <li>
                                            <span>Wielkość filiżanki</span><p>zbożowe/kakao</p>
                                        </li> 
                                        <li>
                                            <span>Główne nuty aromatyczne</span> <i></i>
                                        </li>

                                        <li>
                                            <span>Goryczka </span> <p>***</p>  
                                        <li>
                                            <span>Kwaskowatość </span> <p>**</p> 
                                        </li> 
                                        <li>
                                            <span>Palenie</span><p>*****</p>
                                        </li> 
                                        <li>
                                            <span>Body</span><p>*</p>
                                        </li>    
                                    </ul>
                                    <button class="btn btn-custom btn-white">PRZEJDŹ DO SKLEPU</button>
							    </div>
							    <div role="tabpanel" class="tab-pane" id="profile">
                                    <p>Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża 
                                    z doskonale zbilansowanym głównym bukietem<br />Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża 
                                    z doskonale zbilansowanym głównym bukietem</p>
                                </div>
							    <div role="tabpanel" class="tab-pane" id="messages">
                                    <p>
                                        Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża z doskonale zbilansowanym głównym bukietem
                                    </p>                     
                                </div> 
							  </div> 
						</div>
					</div>
			  	</div>
			  </div>
            </div>
        </div> <!-- CONTAINER END -->
    </section>

        
    <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


    </body>
</html> 
