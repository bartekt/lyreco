<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Kawa</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
    
    <body class="single-akcesories">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
               <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
    

    <section id="subPageContent">
        <div class="container">  
            <div class="row">  
			  <div class="single-box">
			  	<div class="col-md-6">
				  	<div class="product_img">
				  		<div><img src="img/assets/assets_item/c1.png" alt=""></div>
				  	</div>
			  	</div>
			  	<div class="col-md-6 ">
					<div class="description">
						<div>
							<h1>Zestaw 12 filiżanek LUNGO profesional</h1>
							<p>
								Zestaw 12 porcelanowych filiżanek do Cappuccino.<br />
								Pojemność: 170ml. Spodeczki <br />
								(o numerze ref. 5214/12) sprzedawane osobno
							</p>
							<button class="btn btn-custom btn-white">PRZEJDŹ DO SKLEPU</button>
							<div class="row">
								
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="small-product-box">
										<a href="#">  
											<h3>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</h3>
										</a>
										<img src="img/assets/assets_item/c2.png" alt="">
									</div>
								</div>

								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="small-product-box">
										<a href="#">  
											<h3>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</h3>
										</a>
										<img src="img/assets/assets_item/c2.png" alt="">
									</div>
								</div>

							</div>
						</div>
					</div>
			  	</div>
			  </div>
            </div>
        </div> <!-- CONTAINER END -->
    </section>

        
    <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


    </body>
</html> 
