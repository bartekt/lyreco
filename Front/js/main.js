 
// NAVIGATION 

$(document).ready(function () {


        //stick in the fixed 100% height behind the navbar but don't wrap it
        $('#slide-nav.navbar .container').append($('<div id="navbar-height-col"></div>'));

        // Enter your ids or classes
        var toggler = '.navbar-toggle';
        var pagewrapper = '#page-content'; 
        var menuwidth = '100%'; // the menu inside the slide menu itself
        var slidewidth = '80%';
        var menuneg = '-100%';
        var slideneg = '-80%';


        $("#slide-nav").on("click", toggler, function (e) {

            var selected = $(this).hasClass('slide-active');

            $('#slidemenu').stop().animate({
                left: selected ? menuneg : '0px'
            });

            $('#navbar-height-col').stop().animate({
                left: selected ? slideneg : '0px'
            });

            $(pagewrapper).stop().animate({
                left: selected ? '0px' : slidewidth
            }); 

            $(this).toggleClass('slide-active', !selected);
            $('#slidemenu').toggleClass('slide-active'); 

            $('#page-content ').toggleClass('slide-active');


        });


        var selected = '#page-content';  

});


$(".search-open").click(function(){
    $("#main-search-filed").toggle();
});


$(document).ready( function() {
    $('#tabSlider').carousel({
      
        interval:   false,
        pause: true 
	});
	
	var clickEvent = false;
	$('#tabSlider').on('click', '.nav a', function() {
			clickEvent = true;
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
});

//Add Hover effect to menus
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).addClass('open');
  jQuery(this).find('.dropdown-menu').stop(true, true) .show();
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).hide();
  jQuery(this).removeClass('open');
});



// Article HOME Swipe 

 

$('.guides').owlCarousel({ 
    items:4,
    loop:true,
    dots: false,
    margin:20, 
    responsive:{
        230:{
            items:1,
            dots: true,
        },
        300:{
            items:1,
            dots: true,
        },
        480:{
            items:1,
            
        },
        600:{
            items:2,
        },
        1000:{
            items:4,
        }
    } 
}); 
 

$('#myCarousel').on('slid.bs.carousel', function () {
  if($('#slide2').hasClass('active')){
      $('body').addClass('redElements');
  }else{
    $('body').removeClass('redElements');
  }
    if($('#slide3').hasClass('active')){
      $('body').addClass('whiteElements');
  }else{
    $('body').removeClass('whiteElements');
  }
})