<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Lyreco Home</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico --> 

	<script src="js/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    
    <!--
    [if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
    -->

	</head>
	
	<body> 

    <!-- PageTop
    ================================================== --> 
    
      
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>

        <!-- SliderWrap
        ================================================== -->  

       <section class="coffee-slider">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->


  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">  
            <div class="item slide1 active"> 
              <div class="carousel-caption">
               <div class="col-md-5">
                    <div class="sliderExert">
                        <h1 class="clipped">Jenkins oNk</h1>
                        <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami.</p>
                        <button class="btn btn-custom btn-white">POZNAJ OFERTĘ</button>
                    </div>
              </div>
               <div class="col-md-7">
                   <div class="img-box">
                       <img src="img/assets/augila.png" alt="tech 1">
                   </div> 
              </div>
              </div>
            </div> 

            <div class="item slide2"> 
              <div class="carousel-caption">
               <div class="col-md-5">
                    <div class="sliderExert">
                        <div class="sliderExert">
                            <h1>Tyle wspaniałych pomysłów zrodziło się przy filiżance dobrego espresso</h1> 
                            <button class="btn btn-custom btn-white">POZNAJ NASZE KAWY</button>
                        </div>
                    </div>
              </div>
               <div class="col-md-7"> 
                   <div class="img-box">
                        <img src="img/assets/coffee_cup.png" alt="tech 1">
                   </div> 
              </div>
              </div>
            </div>

            <div class="item slide3"> 
              <div class="carousel-caption">
               <div class="col-md-5">
                    <div class="sliderExert">
                        <div class="sliderExert">
                            <h1>W jaki sposób <span class="clipped_in">Nespresso</span> może pomóc przekazać wartości Twojej firmy?</h1>
                            <button class="btn btn-custom btn-white">SPOTKAJMY SIĘ PRZY KAWIE</button>
                        </div>
                    </div>
              </div>
               <div class="col-md-7">
                   <img src="img/assets/info_graphic.png" alt="tech 1">
              </div>
              </div>
            </div>

          <div id="sliderNav" class="cd-slider-nav">
          <nav>
               <a href="#myCarousel"   data-slide="prev" class="prev"></a> 
               <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li> 
               </ol>
               <a href="#myCarousel" role="button" data-slide="next" class="next"></a> 
            </nav>
          </div>

       <!--  <div id="sliderNav" class="cd-slider-nav">
            <nav>
                <a href="#myCarousel" role="button" data-slide="prev" class="prev"></a> 
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li> 
                      </ol>
                <a href="#myCarousel" role="button" data-slide="next" class="next"></a>  
            </nav> 
        </div>  <!-- .cd-slider-nav --> 

        </section> <!-- .cd-hero --> 
    </section> <!--EndSliderWrapper -->

    <!-- About (sectionMiddle)
    ================================================== -->

    <section id="about">
        <div class="container">
            <div class="row">
                <h2>Dla najbardziej wymagających podniebień</h2>
                <div class="col-md-6">
                
                    <div class="box-xl"> 
                        <div class="box-blContent">
                            <h3>Odkryj jedenaście wyjątkowych kaw <span>Grand Cru</span></h3> 
                            <p>Każda z mieszanek Nespresso Grand Cru posiada własną osobowość, odmienne aromaty i skrywa w sobie duszę miejsca, z którego pochodzi. Mieszanki Grand Cru podzielone są według wielkości filiżanki oraz poziomu intensywności. </p> 
                            <p>Delektuj się czterema rodzajami kawy: ristretto, espresso, lungo lub bezkofeinowymi</p> 
                            <button class="btn btn-custom btn-brown">ZAMÓW KAPSUŁKI</button>  
                        </div>  
                    </div>

                     <div class="imgCup">
                        <img src="img/about_bg.png" alt="">
                    </div>

                </div>
                <div class="col-md-6"> 
                    <div class="row">
                        <div class="col-md-6">

                            <div class="box-s border">
                                <img src="img/assets/about_img_1.jpg" alt="">
                                <span class="apla">
                                    <h3>Tylko najlepszy surowiec i dokładna selekcja</h3>
                                </span>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="box-s">
                                <img src="img/assets/about_img_2.png" alt="">
                                <span class="aplaDark">
                                    <div>
                                        <h3>Świat eleganckiej konsumpcji</h3>
                                    </div>
                                </span>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <div class="box-l">
                                <img src="img/assets/about_img_3.png" alt="">
                                <span class="aplaDark">
                                    <div>
                                        <h3>Niezawodna obsługa i serwis. <span>Lyreco</span> Twój kawowy consierge</h3>
                                        <button class="btn btn-custom btn-white">DOWIEDZ SIĘ</button>
                                    </div>
                                </span>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Article
    ================================================== -->

    <section id="article">
        <div class="container">
            <h2>Aktualności do porannej kawy</h2> 

            <div class="articles">
                <div class="row">
                <div class="col-md-12">
                    <div class="guides">
                    <div class="item">
                        <article>
                            <img src="img/assets/article_1.png" alt="">
                            <span class="cat_1">
                                <h3>Cynamonowy kasztan</h3>
                            </span>
                        </article>
                    </div>

                    <div class="item">
                        <article>
                            <img src="img/assets/article_2.png" alt="">
                            <span class="cat_2">
                                <h3>Ciocolatto cafe</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                            <img src="img/assets/article_3.png" alt="">
                             <span class="cat_3">
                                <h3>Poznaj tajniki pracy naszego baristy Bartka</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                          <img src="img/assets/article_4.png" alt="">
                            <span class="cat_4">
                                <h3>Zobacz jaką kawę piją na drugim końcu świata</h3>
                            </span>
                        </article>
                    </div>
                    <div class="item">
                        <article>
                            <img src="img/assets/article_1.png" alt="">
                            <span class="cat_1">
                                <h3>Cynamonowy kasztan</h3>
                            </span>
                        </article>
                    </div>

                    <div class="item">
                        <article>
                            <img src="img/assets/article_2.png" alt="">
                            <span class="cat_2">
                                <h3>Ciocolatto cafe</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                            <img src="img/assets/article_3.png" alt="">
                             <span class="cat_3">
                                <h3>Poznaj tajniki pracy naszego baristy Bartka</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                          <img src="img/assets/article_4.png" alt="">
                            <span class="cat_4">
                                <h3>Zobacz jaką kawę piją na drugim końcu świata</h3>
                            </span>
                        </article>
                    </div>  
                    </div>
                </div>  
                </div>
            </div> 
        </div> 
    </section>

 <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster --> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script> 
    </body>
</html> 

