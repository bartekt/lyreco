<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Kawa</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
    
    <body class="about-lyreco">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper"> 
    
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-3">
 							<h1 class="line">Lyreco</h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container">
         	<div class="row"> 
	        	<div class="col-md-12"> 
					<h2><span>Serwis od początku do końca dla Twojej firmy.</span></h2>
					<p>
						Aby zaspokoić indywidualne potrzeby biznesowe Twojej firmy, Nespresso oferuje pełen zakres usług dla biznesu.<br />
						Wiedza i doświadczenie przeszkolonych Specjalistów ds. Kawy Nespresso pozostają zawsze do Twojej dyspozycji.     
					</p>
	            </div> <!-- ITEM ROW END -->
	        </div>
            <div class="row"> 

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Wyspecjalizowane usługi<br > dla klientów</span></h3>
							<p>
								Nasi Specjaliści ds. Kawy są zawsze gotowi
								do świadczenia zindywidualizowanych
								porad dotyczących naszej kawy, 
								urządzeń, akcesoriów i usług.
							</p>
						</div>
					</a> 
				</div>

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Dostawa kapsułek<br > w 48 godzin</span></h3>
							<p>
								Zamów swoje kapsułki Nespresso
								telefonicznie lub przez Internet,
								a otrzymasz je w ciągu dwóch
								dni roboczych.
							</p>
						</div>
					</a> 
				</div>
				 

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Przyjazna<br > klientom infolinia</span></h3>
							<p>
								Specjaliści ds. Kawy Nespresso są 
								w zasięgu jednego telefonu. Służą 
								wszelką pomocą, bez względu na to, 
								czy potrzebujesz wskazówek dotyczących 
								korzystania ze swojego urządzenia, 
								czy pomocy w przypadku problemów 
								technicznych.
							</p>
						</div>
					</a> 
				</div>
				 

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Pełna<br /> obsługa urządzeń</span></h3>
							<p>
								Nespresso może zaoferować pełny
								serwis na miejscu w celu zapewnienia
								możliwie najlepszej kawy przygotowanej
								przy użyciu maszyny Nespresso.
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Wsparcie techniczne</span></h3>
							<p>
								Nasi technicy mogą udzielić wsparcia
								technicznego zgodnie z Twoimi potrzebami,
								wraz z pełnym zakresem usług
								konserwacyjnych na miejscu.
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Wypożyczenie<br /> zastępczej maszyny <br />na czas naprawy</span></h3>
							<p>
								Nespresso zobowiązuje się 
								do zapewnienia urządzenia zastępczego, 
								aby utrzymać nieprzerwaną możliwość
								oferowania kawy najwyższej jakości, 
								gdy Twoje urządzenie jest w serwisie
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Gwarancja<br />serwisowa</span></h3>
							<p>
								Klientowi Nespresso przysługuje
								standardowa gwarancja dla urządzenia
								oraz trzymiesięczna gwarancja
								dla wszystkich prac serwisowych.
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box active">
							<h3><span>Wypożyczenie<br /> urządzenia<br /> na specjalneokazje</span></h3>
							<p>
								Z przyjemnością wypożyczymy 
								dodatkowe urządzenie na specjalne 
								okazje. Dni otwarte, seminaria, konferencje 
								– niezależnie od okazji, pomożemy 
								w zapewnieniu Twoim Klientom 
								lub partnerom handlowym wyjątkowych
								kaw Nespresso Grand Cru.
							</p>
						</div>
					</a> 
				</div>
				 

            </div>
        </div> <!-- CONTAINER END -->
    </section>

        
    <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


    </body>
</html> 
