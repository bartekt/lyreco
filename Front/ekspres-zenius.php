<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Zenusi</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
	
	<body class="zenius">
	<!-- PageTop
    ================================================== --> 
	<section class="sliderWraper">
		        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
		
        <div class="staticCover">
        	<div class="container">
        	    <!-- Breadcrumbs --> 
				<div class="breadcrumbs">
					<ul>
						<li> <a href="index.html">Home</a> </li>
						<li> <a href="">Ekspres</a> </li>
						<li> <span>Zenius</span> </li>
					</ul>
				</div>
	        	<div class="staticCoverWrapper">
	        		<div class="row">
	        			<div class="col-md-8">
	        				<img src="img/assets/zenius.png">
	        			</div>
	        			<div class="col-md-4">
		        			<div class="desc pull-right">
		        				<div class="title"><h1>Zenius</h1></div>
		        				<p>Gemini 220 od Nespresso przygotuje doskonałe przepisy kawowe na bazie mleka dla Twoich klientów i pracowników. Wyposażona 
w podwójny system ekstrakcyjny i funkcję spieniania mleka, ta maszyna to idealne rozwiązanie dla miłośników kawy latte. Gemini 220 przygotowuje doskonałe Cappuccino 
i Latte Macchiato. Po prostu wybierz z trzech dostępnych rozmiarów kaw: ristretto, espresso lub lungo, a maszyna zajmie się resztą. </p>
		        			</div> 
	        			</div>
	        		</div> 
	        	</div> 
        	</div> 
        </div> 
	</section>
	
	<!-- SubPageContent
    ================================================== -->
	
	<section id="subPageContent">
		<div class="container">
			<div id="contactLine">
				<div class="row">
					<div class="col-md-8">
						<h4>KONTAKT</h4>
						<div class="pill-left"><span>Dla biur:</span><a href="tel:801307007">801 30 70 07</a></div>
						<div class="pull-right"><span>Dla restauracji, hoteli, kawiarni:</span><a href="mail:kawa@lyreco.com">kawa@lyreco.com</a></div>
					</div>
					<div class="col-md-4">
						<div class="pull-right">
							<button class="btn btn-custom btn-dark">NAPISZ DO NAS</button>
						</div>
					</div>
				</div>
			</div> 
		</div>
		<div class="dotPattern"> 
			<div class="container"> 
				<h4>Charekterystyka</h4> 
				<div class="wrap">
					<ul>
						<li><i></i><span>Gorąca woda</span></li>
						<li><i></i><span>Automatyczna <br />kontrola objętościfiliżanki</span></li>
						<li><i></i><span>Tryb oszczędzania energii</span></li>
						<li><i></i><span>Ristretto espresso i lungo</span></li>
						<li><i></i><span>Przepisy z gorącym i zimnym mlekiem</span></li>
						<li><i></i><span>Przygotowanie mleka za jednym dotknięciem przycisku</span></li>
						<li><i></i><span>Funkcja przygotowania Cappuciino, Caffe Latte i Latte Macchiato</span></li>
						<li><i></i><span>Możliwość zaprogramowania cyfrowego wyświetlacza na kilka języków</span></li>
						<li><i></i><span>Bezpośrednie połączenie do źródła wody</span></li>
						<li><i></i><span>Możliwość zaprogramowania wielkośći filiżanki</span></li>
					</ul> 
				</div>
			</div>
		</div>

		<div class="container">
			<h4>Szczegóły</h4>
			<div class="wrap details"> 
                <div class="firstRow">
                    <div class="boxSmaller border-start">
                        <ul>
                            <li>Moc (w wattach) : 9000 Watt</li>
                            <li>Ciśnienie : 13-16 Bars</li>
                            <li>Waga : 100 Kilogramów</li>
                            <li>Wymiary (szer. x gł. x wys.) : 100x 62 x 63 cm</li>
                            <li>Liczba boilerów (element grzejny) : 6</li>
                            <li>Pojemność pojemnika na zużyte kapsułki : 250</li>
                            <li>Extraction Heads : 4</li>
                            <li>Alarm odkamieniania</li>
                            <li>Tryb energooszczędny</li>
                            <li>Gwarancja : 2 lata</li>
                        </ul>
                    </div>
                    <div class="boxSmaller">
                        <img src="img/assets/details_zenius_img1.png" alt=""> 
                    </div>
                    <div class="boxBigger border-end">
                          <ul> 
                            <li>Pół-automatyczna funkcja czyszczenia</li>
                            <li>Alarm wymiany filtra wody</li>
                            <li>Przygotowanie przepisów za jednym 
                            dotknięciem przycisku (przepisy mleczne)</li>
                            <li>Elektroniczne powiadomienie o zapełnieniu 
                            pojemnika na zużyte kapsułki</li>
                            <li>Regulowana podstawka pod wysokie 
                            szklanki, np. Latte Macchiato</li>
                            <li>Wbudowany pojemnik (5L) na mleko 
                            schładzany do temperatury 4°C</li>
                            <li>Możliwość zaprogramowania maszyny 
                            w kilku językach</li>
                        </ul>
                    </div>
                </div> 
            <div>
                <div class="boxSmaller">
                    <img src="img/assets/details_zenius_img2.png" alt="">
                </div>
                <div class="boxSmaller border-center middleBox">
                    <ul> 
                        <li>Maszyna podłączona do sieci</li>
                        <li>Bezpośrednie podłączenie do źródła wody</li>
                        <li>Manualne wkładanie i usuwanie kapsułki</li>
                        <li>Tryb chroniący przed awarią każdą z głowic</li>
                        <li>Pół-automatyczna funkcja czyszczenia</li>
                        <li>Cyfrowy wielojęzyczny wyświetlacz</li>
                    </ul>
                </div>
                <div class="boxBigger lastBox">
                    <img src="img/assets/details_zenius_img3.png" alt="">
                </div>
            </div> 
			</div>
		</div>

		<div class="container">
			<div id="contactLine">
				<div class="row">
					<div class="col-md-8">
						<h4>KONTAKT</h4>
						<div class="pill-left"><span>Dla biur:</span><a href="tel:801307007">801 30 70 07</a></div>
						<div class="pull-right"><span>Dla restauracji, hoteli, kawiarni:</span><a href="mail:kawa@lyreco.com">kawa@lyreco.com</a></div>
					</div>
					<div class="col-md-4">
						<div class="pull-right">
							<button class="btn btn-custom btn-dark">NAPISZ DO NAS</button>
						</div>
					</div>
				</div>
			</div> 
		</div>

	</section>

    <!-- FooterArea
    ================================================== --> 

	<footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

 	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


	</body>
</html> 