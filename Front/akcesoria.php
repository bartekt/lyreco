<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Kawa</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
    
    <body class="akcesories">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
                <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-3">
 							<h1 class="line">Akcesoria</h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container"> 
            <div class="row">
	        	<div class="col-md-12 nav-box-row">
					<ul class="box-nav">
	        			<li><a href="#">Degustacja kawy</a></li>
	        			<li class="active"><a href="#">Rozwiązania mleczn</a></li>
	        			<li><a href="#">Pojemniki na kapsułki</a></li>
	        			<li><a href="#">Kawa i dodatki </a></li> 
	        			<li><a href="#">Utrzymanie i konserwacja</a></li>
	        		</ul>
					<div class="col-md-12">
					</div>
	        	</div> 
        	</div> 
            <div class="row">  
				<div class="col-md-3 item-akcesories"> 
					<a href="#">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
            </div>
        </div> <!-- CONTAINER END -->
    </section>

        
    <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


    </body>
</html> 
