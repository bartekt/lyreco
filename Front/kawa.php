<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Description Here">
    <meta name="author" content="Author Name">  

    <title>Kawa</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">  
    <!-- Custom styles for this template --> 
    <link href="css/main.css" rel="stylesheet"> 
    
    <!-- Font OpenSams -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,300italic,300' rel='stylesheet' type='text/css'> 
    <!-- Font Tinos -->
    <link href='https://fonts.googleapis.com/css?family=Tinos:400,700' rel='stylesheet' type='text/css'>
    <!-- Font Lato -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,100&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Font Awsome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Font Nothing You Could Do -->
    <link href='https://fonts.googleapis.com/css?family=Nothing+You+Could+Do' rel='stylesheet' type='text/css'> 
    <!-- Favico -->
   
  
	<script src="js/modernizr.custom.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	</head>
    
    <body class="categoryKawa">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
                <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php require ("partials/menu.php"); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-4">
                            <h1>
                               Odkryj jedenaście wyjątkowych<br />kaw <span>Grand Cru</span>
                            </h1> 
                            <p>
                            	Każda z kaw Nespresso Grand Cru ma swoją własną osobowość, wyrafinowany charakter,
                                a także skrywa duszę miejsca, z którego pochodzi. Zostały one skategoryzowane według wielkości filiżanki oraz intensywności. 
                                <br /><br />
                                W ten sposób wyróżniamy cztery grupy kaw: Ristretto (25 ml), Espresso (40 ml), Lungo (110 ml) oraz kawy bezkofeinowe (40 lub 110 ml)
                            </p>
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container"> 
            <div class="row"> 
                <div class="col-md-12 nav-box-row">
                    <ul class="box-nav">
                        <li><a href="#">RISTRETTO</a></li>
                        <li class="active"><a href="#">ESPRESSO</a></li>
                        <li><a href="#">LUNGO</a></li>
                        <li><a href="#">Decaffeinato</a></li>   
                    </ul>
                </div> 
            </div>
            
            <div class="row coffe-items">

                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Ristretto Intenso</h3>
                            <img src="img/assets/assets_item/d1.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                </ul>
                                <em>12</em>
                                <i class="size-s"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item selected-espresso">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Espresso Forte</h3>
                            <img src="img/assets/assets_item/d2.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <em>7</em>
                                <i class="size-s"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Lungo Forte</h3>
                            <img src="img/assets/assets_item/d3.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <em>6</em>
                                <i class="size-m"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Espresso Decaffeinato</h3>
                            <img src="img/assets/assets_item/d4.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li> 
                                    <li></li>
                                </ul>
                                <em>11</em>
                                <i class="size-l"></i>
                            </div>
                        </div>
                    </a>
                </div> 

                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Ristretto Intenso</h3>
                            <img src="img/assets/assets_item/d5.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                </ul>
                                <em>12</em>
                                <i class="size-s"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item selected-espresso">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Espresso Forte</h3>
                            <img src="img/assets/assets_item/d6.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <em>7</em>
                                <i class="size-m"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Lungo Forte</h3>
                            <img src="img/assets/assets_item/d7.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <em>6</em>
                                <i class="size-l"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Espresso Decaffeinato</h3>
                            <img src="img/assets/assets_item/d8.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li> 
                                    <li></li>
                                </ul>
                                <em>11</em>
                                <i class="size-s"></i>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Ristretto Intenso</h3>
                            <img src="img/assets/assets_item/d9.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                </ul>
                                <em>12</em>
                                <i class="size-m"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item selected-espresso">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Espresso Forte</h3>
                            <img src="img/assets/assets_item/d10.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <em>7</em>
                                <i class="size-l"></i>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 item">
                    <a href="kawa-single.php">
                        <div class="box">
                            <h3>Lungo Forte</h3>
                            <img src="img/assets/assets_item/d11.png" alt="">
                            <div>
                                <ul>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li class="active"></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <em>6</em>
                                <i class="size-s"></i>
                            </div>
                        </div>
                    </a>
                </div> 

            </div>

            <div class="row">
                <div class="col-md-12 box-more">
                    <div class="more-info">
                        <div class="col-md-3">
                            <img src="img/assets/assets_item/e1.png" alt="">
                            <div class="foot">
                              <ul>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li class="active"></li>
                                <li></li>
                                <li></li>
                                <li></li> 
                            </ul>
                            <em>9</em>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <h2>Espresso Origin Brazil</h2>
                            <p>Czysta kawa Arabika. Espresso Origin Brazil to delikatna 
                            mieszanka o gładkiej konsystencji i słodkim, łagodnym smaku 
                            wzbogaconym nutami lekko palonego ziarna. Najlepiej smakuje 
                            podana jako espresso (40 ml).</p>
                            <ul>
                                <li>Espresso</li>
                                <li>Profil aromatyczny: słodka, zbożowa</li>
                            </ul>
                        </div>
                    </div>    
                </div>
            </div>

        </div> 
    </section>
    
    <section class="how-it-works">
        <div class="box">
                <h3>JAK DELEKTOWAĆ SIĘ <br/>KAWĄ  <em>NESPRESSO</em></h3>
        <p>Wpierw wybierasz swoją kawę Grand Cru, następnie zachwycasz się jej wyglądem i aromatem, aż wreszcie... 
zaczynasz się nim delektować. Poznaj sztukę delektowania się kawą według Nespresso.</p>    
        </div> 
    </section>

    <section class="tab-slider"> 

        <div id="tabSlider" class="carousel slide" data-ride="carousel">
            
            <div class="nav-box">
                <ul class="nav nav-pills">
                    <li data-target="#tabSlider" data-slide-to="0" class="active"><a href="#"><i></i><small>Kawy Ristretto</small></a></li>
                    <li data-target="#tabSlider" data-slide-to="1"><a href="#"><i></i><small>Kawy Espresso</small></a></li>
                    <li data-target="#tabSlider" data-slide-to="2"><a href="#"><i></i><small>Kawy Lungo</small></a></li>
                    <li data-target="#tabSlider" data-slide-to="3"><a href="#"><i></i><small>Kawa z mlekiem</small></a></li>
                </ul>
            </div>

            <div class="carousel-wrap">
                 <div class="carousel-box">
                    <div class="container">
                        <div class="carousel-inner">  
                            <div class="item active">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon">
                                            <img src="img/assets/assets_item/tab-slider-icon-big.png" alt="">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <h2>Ristretto <span>25 ml</span></h2>
                                        <p>
                                        Silne i kontrastujące. Bogaty smak prawdziwej kawy Ristretto to kwintensencja kawowego orzeźwienia.
                                        <br /> 
                                        Ciesz się kawą Ristretto rano lub po posiłku: mieszanki Ristretto od Nespresso to kombinacja intensywnego smaku z silną osobowością. Podawaj w typowej, małej filiżance by osiagnąć efekt ristretto w najczystszej postaci.
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="img/assets/assets_item/cup-ristretto.png" alt="">
                                    </div>
                                </div> 
                            </div><!-- End Item --> 
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon">
                                            <img src="img/assets/assets_item/tab-slider-icon-big.png" alt="">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <h2>Espresso <span>25 ml</span></h2>
                                        <p>
                                        Silne i kontrastujące. Bogaty smak prawdziwej kawy Ristretto to kwintensencja kawowego orzeźwienia.
                                        <br /> 
                                        Ciesz się kawą Ristretto rano lub po posiłku: mieszanki Ristretto od Nespresso to kombinacja intensywnego smaku z silną osobowością. Podawaj w typowej, małej filiżance by osiagnąć efekt ristretto w najczystszej postaci.
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="img/assets/assets_item/cup-ristretto.png" alt="">
                                    </div>
                                </div> 
                            </div><!-- End Item -->
                            <div class="item ">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon">
                                            <img src="img/assets/assets_item/tab-slider-icon-big.png" alt="">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <h2>Lungo <span>25 ml</span></h2>
                                        <p>
                                        Silne i kontrastujące. Bogaty smak prawdziwej kawy Ristretto to kwintensencja kawowego orzeźwienia.
                                        <br /> 
                                        Ciesz się kawą Ristretto rano lub po posiłku: mieszanki Ristretto od Nespresso to kombinacja intensywnego smaku z silną osobowością. Podawaj w typowej, małej filiżance by osiagnąć efekt ristretto w najczystszej postaci.
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="img/assets/assets_item/cup-ristretto.png" alt="">
                                    </div>
                                </div> 
                            </div><!-- End Item -->
                            <div class="item ">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="icon">
                                            <img src="img/assets/assets_item/tab-slider-icon-big.png" alt="">
                                        </div> 
                                    </div>
                                    <div class="col-md-6">
                                        <h2>Kawa z mlekiem <span>25 ml</span></h2>
                                        <p>
                                        Silne i kontrastujące. Bogaty smak prawdziwej kawy Ristretto to kwintensencja kawowego orzeźwienia.
                                        <br /> 
                                        Ciesz się kawą Ristretto rano lub po posiłku: mieszanki Ristretto od Nespresso to kombinacja intensywnego smaku z silną osobowością. Podawaj w typowej, małej filiżance by osiagnąć efekt ristretto w najczystszej postaci.
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="img/assets/assets_item/cup-ristretto.png" alt="">
                                    </div>
                                </div>  
                            </div><!-- End Item --> 
                        </div> 
                    </div>
                </div> 
            </div>

        </div> 

    </section> 
        
    <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>

    <script src="js/main.js"></script> 


    </body>
</html> 
