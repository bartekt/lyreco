<?php /* Template Name: Akcesoria */ get_header(); ?>
 <body class="akcesories">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
                <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-3">
 							<h1 class="line">Akcesoria</h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container"> 
            <div class="row">
	        	<div class="col-md-12 nav-box-row">
					<ul class="box-nav">
	        			<li><a href="#">Degustacja kawy</a></li>
	        			<li class="active"><a href="#">Rozwiązania mleczn</a></li>
	        			<li><a href="#">Pojemniki na kapsułki</a></li>
	        			<li><a href="#">Kawa i dodatki </a></li> 
	        			<li><a href="#">Utrzymanie i konserwacja</a></li>
	        		</ul>
					<div class="col-md-12">
					</div>
	        	</div> 
        	</div> 
            <div class="row">  
				<div class="col-md-3 item-akcesories"> 
					<a href="#">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
				<div class="col-md-3 item-akcesories"> 
					<a href="akcesoria-single.php">
						<div class="box">
							<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
							<img src="img/assets/assets_item/b1.png" alt="">
							<span class="apla">
								<div>
									<h3><span>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</span></h3>
									<p>Porcelanowa filiżanka do Cappuccino, 
									idealna do serwowania kawy Cappuccino 
									z puszystą, mleczną pianką.</p>
									<i><span class="fa fa-angle-right"></span></i>
								</div> 
							</span>
						</div>
					</a> 
				</div>
            </div>
        </div> <!-- CONTAINER END -->
    </section>
<?php get_footer(); ?>