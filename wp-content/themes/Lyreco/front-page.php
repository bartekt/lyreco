<?php get_header(); ?>

<?php get_template_part( 'menu' ); ?>

<section class="coffee-slider">
<div id="myCarousel" class="carousel slide" data-ride="carousel"> 
<section class="coffee-slider">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->


  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">  
            <div id="slide1" class="item slide1 active"> 
              <div class="carousel-caption">
               <div class="col-md-5">
                    <div class="sliderExert">
                        <h1 class="clipped">Aguila</h1>
                        <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami.</p>
                        <button class="btn btn-custom btn-white">POZNAJ OFERTĘ</button>
                    </div>
              </div>
               <div class="col-md-7">
                   <div class="img-box">
                       <img src="<?php echo get_template_directory_uri(); ?>/img/assets/augila.png" alt="tech 1">
                   </div> 
              </div>
              </div>
            </div> 

            <div id="slide2" class="item slide2"> 
              <div class="carousel-caption">
               <div class="col-md-5">
                    <div class="sliderExert">
                        <div class="sliderExert">
                            <h1>Tyle wspaniałych pomysłów zrodziło się przy filiżance dobrego espresso</h1> 
                            <button class="btn btn-custom btn-white">POZNAJ NASZE KAWY</button>
                        </div>
                    </div>
              </div>
               <div class="col-md-7"> 
                   <div class="img-box">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/assets/coffee_cup.png" alt="tech 1">
                   </div> 
              </div>
              </div>
            </div>

            <div id="slide3" class="item slide3"> 
              <div class="carousel-caption">
               <div class="col-md-5">
                    <div class="sliderExert">
                        <div class="sliderExert">
                            <h1>W jaki sposób <span class="clipped_in">Nespresso</span> może pomóc przekazać wartości Twojej firmy?</h1>
                            <button class="btn btn-custom btn-white">SPOTKAJMY SIĘ PRZY KAWIE</button>
                        </div>
                    </div>
              </div>
               <div class="col-md-7">
                    <div class="img-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/assets/info_graphic.png" alt="tech 1">
                    </div>
              </div>
              </div>
            </div>

          <div id="sliderNav" class="cd-slider-nav">
          <nav>
               <a href="#myCarousel" data-slide="prev" class="prev"></a> 
               <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li> 
               </ol>
               <a href="#myCarousel" role="button" data-slide="next" class="next"></a> 
            </nav>
          </div> 
      
        </section> <!-- .cd-hero --> 
    </section> <!--EndSliderWrapper -->

    <!-- About (sectionMiddle)
    ================================================== -->

    <section id="about">
        <div class="container">
            <div class="row">
                <h2>Dla najbardziej wymagających podniebień</h2>
                <div class="col-md-6">
                
                    <div class="box-xl"> 
                        <div class="box-blContent">
                            <h3>Odkryj jedenaście wyjątkowych kaw <span>Grand Cru</span></h3> 
                            <p>Każda z mieszanek Nespresso Grand Cru posiada własną osobowość, odmienne aromaty i skrywa w sobie duszę miejsca, z którego pochodzi. Mieszanki Grand Cru podzielone są według wielkości filiżanki oraz poziomu intensywności. </p> 
                            <p>Delektuj się czterema rodzajami kawy: ristretto, espresso, lungo lub bezkofeinowymi</p> 
                            <button class="btn btn-custom btn-brown">ZAMÓW KAPSUŁKI</button>  
                        </div>  
                    </div>

                     <div class="imgCup">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/about_bg.png" alt="">
                    </div>

                </div>
                <div class="col-md-6"> 
                    <div class="row">
                        <div class="col-md-6">

                            <div class="box-s border">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/assets/about_img_1.jpg" alt="">
                                <span class="apla">
                                    <h3>Tylko najlepszy surowiec i dokładna selekcja</h3>
                                </span>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="box-s">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/assets/about_img_2.png" alt="">
                                <span class="aplaDark">
                                    <div>
                                        <h3>Świat eleganckiej konsumpcji</h3>
                                    </div>
                                </span>
                            </div>    
                        </div>
                        <div class="col-md-12">
                            <div class="box-l">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/assets/about_img_3.png" alt="">
                                <span class="aplaDark">
                                    <div>
                                        <h3>Niezawodna obsługa i serwis. <span>Lyreco</span> Twój kawowy consierge</h3>
                                        <button class="btn btn-custom btn-white">DOWIEDZ SIĘ</button>
                                    </div>
                                </span>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Article
    ================================================== -->

    <section id="article">
        <div class="container">
            <h2>Aktualności do porannej kawy</h2> 

            <div class="articles">
                <div class="row">
                <div class="col-md-12">
                    <div class="guides">
                    <div class="item">
                        <article>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_1.png" alt="">
                            <span class="cat_1">
                                <h3>Cynamonowy kasztan</h3>
                            </span>
                        </article>
                    </div>

                    <div class="item">
                        <article>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_2.png" alt="">
                            <span class="cat_2">
                                <h3>Ciocolatto cafe</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_3.png" alt="">
                             <span class="cat_3">
                                <h3>Poznaj tajniki pracy naszego baristy Bartka</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                          <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_4.png" alt="">
                            <span class="cat_4">
                                <h3>Zobacz jaką kawę piją na drugim końcu świata</h3>
                            </span>
                        </article>
                    </div>
                    <div class="item">
                        <article>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_1.png" alt="">
                            <span class="cat_1">
                                <h3>Cynamonowy kasztan</h3>
                            </span>
                        </article>
                    </div>

                    <div class="item">
                        <article>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_2.png" alt="">
                            <span class="cat_2">
                                <h3>Ciocolatto cafe</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_3.png" alt="">
                             <span class="cat_3">
                                <h3>Poznaj tajniki pracy naszego baristy Bartka</h3>
                            </span>
                        </article>
                    </div> 

                    <div class="item">
                        <article>
                          <img src="<?php echo get_template_directory_uri(); ?>/img/assets/article_4.png" alt="">
                            <span class="cat_4">
                                <h3>Zobacz jaką kawę piją na drugim końcu świata</h3>
                            </span>
                        </article>
                    </div>  
                    </div>
                </div>  
                </div>
            </div> 
        </div> 
    </section>
<?php get_footer(); ?>