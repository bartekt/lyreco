 <?php get_header(); ?>
 <body class="single-kawa">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
        
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
    

    <section id="subPageContent">
        <div class="container">  
            <div class="row">  
			  <div class="single-box">
			  	<div class="col-md-6">
				  	<div class="product_img">
				  		<div><img src="img/assets/assets_item/espresso_legero.png" alt=""></div>
				  	</div>
			  	</div>
			  	<div class="col-md-6 ">
					<div class="description">
						<div class="tabs">
							<h1>Espresso Leggero</h1>
							<span>LEKKA I ORZEŹWIAJĄCA</span>
							 <!-- Nav tabs -->
							  <ul class="nav nav-tabs" role="tablist">
							    <li role="presentation" class="active">
                    
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Podgląd</a></li>
							    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Szczegóły</a></li>
							    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Składniki</a></li> 
							  </ul>

							  <!-- Tab panes -->
							  <div class="tab-content">
							    <div role="tabpanel" class="tab-pane active" id="home">
									<p>Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża 
									z doskonale zbilansowanym głównym bukietem</p>

                                    <ul>
                                        <li>
                                            <span>Intensywność:</span>
                                            <em>6</em>
                                            <ol>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li class="active"></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                            </ol>
                                        </li>  
                                        <li>
                                            <span>Wielkość filiżanki</span><p>zbożowe/kakao</p>
                                        </li> 
                                        <li>
                                            <span>Główne nuty aromatyczne</span> <i></i>
                                        </li>

                                        <li>
                                            <span>Goryczka </span> <p>***</p>  
                                        <li>
                                            <span>Kwaskowatość </span> <p>**</p> 
                                        </li> 
                                        <li>
                                            <span>Palenie</span><p>*****</p>
                                        </li> 
                                        <li>
                                            <span>Body</span><p>*</p>
                                        </li>    
                                    </ul>
                                    <button class="btn btn-custom btn-white">PRZEJDŹ DO SKLEPU</button>
							    </div>
							    <div role="tabpanel" class="tab-pane" id="profile">
                                    <p>Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża 
                                    z doskonale zbilansowanym głównym bukietem<br />Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża 
                                    z doskonale zbilansowanym głównym bukietem</p>
                                </div>
							    <div role="tabpanel" class="tab-pane" id="messages">
                                    <p>
                                        Espresso Leggero to wyborna mieszanka południowoamerykańskich kaw Arabika i Robusta, łącząca delikatne nuty kakao i zboża z doskonale zbilansowanym głównym bukietem
                                    </p>                     
                                </div> 
							  </div> 
						</div>
					</div>
			  	</div>
			  </div>
            </div>
        </div> <!-- CONTAINER END -->
    </section>
    <?php get_footer(); ?>