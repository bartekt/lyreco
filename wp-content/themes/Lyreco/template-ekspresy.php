<?php /* Template Name: Ekspresy */ get_header(); ?>
 <body class="categoryEkspress">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
        
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>
                                Nieodłączny element<br />życia każdej firmy
                            </h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container">
            <h4>poznaj nasze ekspresy</h4>
        </div> 
        <div class="container"> 
            <div> 
                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemAugila">
                    
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Augila 420</h2>
                                </div>
                                <div class="col-md-7">
                                     <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                        <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/augila420.png" alt="augila420">
                            <div class="labelTriangle">
                                <div> 
                                    <i></i>
                                    <p><small>od</small>100</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>

                </div>

                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemAugila">
                    
                    <div class="row">

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Augila 220</h2>
                                </div>
                                <div class="col-md-7">
                                     <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/augila220.png" alt="augila220">
                            <div class="labelTriangle">
                                <div>
                                    <i></i>
                                    <p><small>od</small>50</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>

                </div>

                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemGemini">

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Gemini 500</h2>
                                </div>
                                <div class="col-md-7">
                                     <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 

                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/gemini223.png" alt="gemini223">
                            <div class="labelTriangle">
                                <div>
                                    <i></i>
                                    <p><small>od</small>20</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>
                </div>

                <!-- ITEM -->
                <div class=" col-md-12 itemBox itemSystemTower">

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>System Tower</h2>
                                </div>
                                <div class="col-md-7">
                                    <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href=""class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 

                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/system_tower1.png" alt="system_tower">
                            <div class="labelTriangle">
                                <div> 
                                    <i></i>
                                    <p><small>od</small>20</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>
                </div>

                <!-- ITEM -->
                <div class="col-md-12 col-sm-12 col-xs-12 itemBox itemsZenius">

                    <div class="row"> 

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-5 headline">
                                    <h2>Zenius</h2>
                                </div>
                                <div class="col-md-7">
                                    <div class="details">
                                         <p>Aguila, doskonałość na wielką skalę. Tradycja zawodowych baristów połączona z kunsztem technologicznym owocuje niezrównanymi doświadczeniami. </p>
                                         <a href="#" class="btn btn-custom btn-dark">SZCZEGÓŁY <i></i></a>
                                    </div>  
                                </div>
                            </div> 
                        </div> 

                        <div class="col-md-6 col-sm-12 col-xs-12 label">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/zenusi.png" alt="zenusi">
                            <div class="labelTriangle">
                                <div>
                                    <i></i>
                                    <p><small>od</small>20<small>do</small>30</p>
                                    <span>UŻYTKOWNIKÓW</span>
                                </div>
                            </div>
                            <div class="LabelLine"></div>
                        </div>

                    </div>


                </div>

            </div> <!-- ITEM ROW END -->
        </div> <!-- CONTAINER END -->
    </section>
<?php get_footer(); ?>