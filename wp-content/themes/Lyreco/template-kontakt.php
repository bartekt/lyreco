<?php /* Template Name: Kontakt */ get_header(); ?>
 <body class="kontakt">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper"> 
    
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
 		<?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
		<div id="map"></div>  
    
    </section>

    <section id="subPageContent">
        <div class="container">
      		<div class="row"> 
      			<div class="col-md-6 col-sm-6 col-xs-12">
      				<h3>CONTACT US</h3>
      				<p>Etiam convallis, felis quis dapibus dictum, libero mauris luctus nunc, non fringilla purus ligula non justo. Nullam elementum consequat lacus, sit amet pulvinar urna hendrerit sit amet.</p>

      				<div id="contact-form">
      					<?php echo do_shortcode('[contact-form-7 id="35" title="Formularz 1"]'); ?>
      				</div>
      				

      			</div>
      			<div class="col-md-6 col-sm-6 col-xs-12">
      				<h3>NEED HELP?</h3>

					<p>Don’t hestiate to ask us something. Email us directly info@example.com or call us at 1-202-555-0116. Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. Donec a scelerisque turpis.</p>

					<h3>GIVE US A CALL</h3>

					<p>Etiam convallis, felis quis dapibus dictum, libero mauris luctus nunc, non fringilla purus ligula non justo. Nullam elementum consequat lacus, sit amet pulvinar urna hendrerit sit amet. Interdum et malesuada fames ac ante. Donec a scelerisque turpis, ut porta turpis. Integer imperdiet aliquet.</p>

					<h1><a href="tel:801307007">+48 801 30 70 07</a></h1>
      			</div>	
      		</div>
        </div> <!-- CONTAINER END -->
    </section>
<?php get_footer(); ?>
