<?php wp_footer(); ?>
 <footer>
        <div class="container">
            <div class="row">
                <div class="brand">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/footer_brand.png" alt="">  
                </div> 
                <ul class="bottomNav">
                    <li><a href="#">kontakt</a></li>
                    <li><a href="#">mapa strony</a></li>
                    <li><a href="#">nespresso.com</a></li> 
                    <li><a href="#">regulamin</a></li>
                    <li><a href="#">dla mediów</a></li>
                </ul>
                <div class="creator pull-right">
                    <span>Realizacja:</span>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/img/omega.png" class="img-responsive" alt=""></div>
                </div>
            </div>
        </div>  
    </footer>
 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkDhkcBP6ezMVMCbgjwWKUjykTKS7pYq8&callback=initMap"
    async defer></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> 

        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 12,
                    scrollwheel: false,
                    navigationControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    draggable: false,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(50.0767434, 19.8921973), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles:          
[{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var image = 'http://lyreco.dev/wp-content/themes/lyreco/img/marker.png';
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(50.0767434, 19.8921973),
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: image,
                    title: 'Biuro projektów Michał Strączek ul.Szablowskiego 6/6 30-127 Kraków'
                });
            }

            function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
        </script>

     

        <script>
            var menuRight = document.getElementById( 'cbp-spmenu-s2' ), 
                showRightPush = document.getElementById( 'showRightPush' ),
                body = document.body; 

            showRightPush.onclick = function() {
                classie.toggle( this, 'active' );
                classie.toggle( body, 'cbp-spmenu-push-toleft' );
                classie.toggle( menuRight, 'cbp-spmenu-open' );
                disableOther( 'showRightPush' );
            };

            function disableOther( button ) { 
                if( button !== 'showRightPush' ) {
                    classie.toggle( showRightPush, 'disabled' );
                }
            }




                ( function( $ ) {
                $( document ).ready(function() {
                  
                  $('#nav > ul > li.menu-item-has-children > a').click(function() {
                  $('#nav > ul > li > ul ').addClass('open'); 
                  $('.navbar ').addClass('nav_open');
                  // $(this).closest('li').addClass('active');  
                  var checkElement = $(this).next();
                  if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                    $(this).closest('li').removeClass('open');
                    $('.navbar ').removeClass('nav_open'); 
                    checkElement.slideUp('fast');
                  }
                  if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                    $('#nav ul ul:visible').slideUp('fast');
                    $(this).closest('li').removeClass('active');
                    checkElement.slideDown('fast');
                  }
                  if($(this).closest('li').find('ul').children().length == 0) {
                    return true;
                  } else {
                    return false;   
                  }     
                });
                });
                } )( jQuery ); 

        </script>
        </script> 

</body>
</html>   