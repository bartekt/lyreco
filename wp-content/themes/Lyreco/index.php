<?php get_header(); ?> 
 <body class="aktualnosci">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
               <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-3">
                            <h1 class="line"> Aktualnośći </h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container">
            <div class="row">
                <div class="col-md-12 nav-box-row">
                    <ul class="box-nav">
                        <li><a href="#">Przepisy</a></li>
                        <li><a href="#">Nowości</a></li>
                        <li class="active"><a href="#">Historia</a></li>
                        <li><a href="#">Produkty</a></li>
                    </ul>
                </div> 
            </div> 

            <div class="row"> 

            <!-- ROW --> 
                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a1.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>

                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a2.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>

                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a3.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>
                
                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a4.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>
                <!-- ROW -->

            <!-- ROW --> 
                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a1.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>

                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a2.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>

                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a3.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>
                
                <div class="col-md-3 act-item">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/assets/assets_item/a4.png" alt="">
                            <div>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
                                <p>
                                    In finibus nulla et nisl facilisis dignissim. 
                                    Aliquam sagittis diam sit amet nisl 
                                    vulputate, a ullamcorper mi commodo. 
                                    Duis sed lectus velit.
                                </p>
                            </div>
                        </a>
                    </article>
                </div>
                <!-- ROW -->

            </div> <!-- ITEM ROW END -->
        </div> <!-- CONTAINER END -->
    </section>
<?php get_footer(); ?>
