<?php get_header(); ?>
<body class="single-akcesories">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper">
        
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
    

    <section id="subPageContent">
        <div class="container">  
            <div class="row">  
			  <div class="single-box">
			  	<div class="col-md-6">
				  	<div class="product_img">
				  		<div><img src="img/assets/assets_item/c1.png" alt=""></div>
				  	</div>
			  	</div>
			  	<div class="col-md-6 ">
					<div class="description">
						<div>
							<h1>Zestaw 12 filiżanek LUNGO profesional</h1>
							<p>
								Zestaw 12 porcelanowych filiżanek do Cappuccino.<br />
								Pojemność: 170ml. Spodeczki <br />
								(o numerze ref. 5214/12) sprzedawane osobno
							</p>
							<button class="btn btn-custom btn-white">PRZEJDŹ DO SKLEPU</button>
							<div class="row">
								
								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="small-product-box">
										<a href="#">  
											<h3>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</h3>
										</a>
										<img src="img/assets/assets_item/c2.png" alt="">
									</div>
								</div>

								<div class="col-md-6 col-sm-6 col-xs-6">
									<div class="small-product-box">
										<a href="#">  
											<h3>ZESTAW 12 FILIŻANEK CAPPUCCINO PROFESSIONAL</h3>
										</a>
										<img src="img/assets/assets_item/c2.png" alt="">
									</div>
								</div>

							</div>
						</div>
					</div>
			  	</div>
			  </div>
            </div>
        </div> <!-- CONTAINER END -->
    </section>
<?php get_footer(); ?>