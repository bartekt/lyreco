	     <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="slide-nav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>//img/logo_lyreco.png" alt=""></a>
                    <a class="navbar-brand  brand-nespresso" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo_nespresso.png" alt=""></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
           
               
                 <div id="slidemenu" class="navbar-right">
                        <ul class="nav navbar-nav">
                            <li><a href="/aktualnosci">Aktualności</a></li> 
                            <li class="dropdown">
                                <a href="/ekspresy">Ekspres</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="/ekspresy/aguila"> 
                                            <div class="navThumb ">
                                            <p>Aguila</p>
                                                <span>
                                                     <img src="<?php echo get_template_directory_uri(); ?>/img/menu/img1.png" alt="">
                                                </span>
                                            </div>
                                        </a>
                                    </li> 
                                    <li>
                                        <a href="/ekspresy/seria-gemini"> 
                                            <div class="navThumb">
                                             <p>Seria Gemini</p>
                                                <span>
                                                     <img src="<?php echo get_template_directory_uri(); ?>/img/menu/img2.png" alt="">
                                                </span>
                                            </div>
                                        </a>
                                    </li>                                
                                    <li>
                                        <a href="/ekspresy/zenius"> 
                                            <div class="navThumb">
                                             <p>Zenius</p>
                                                <span>
                                                     <img src="<?php echo get_template_directory_uri(); ?>/img/menu/img3.png" alt="">
                                                </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/ekspresy/system-tower"> 
                                            <div class="navThumb">
                                                <p>System Tower</p>
                                                <span>
                                                     <img src="<?php echo get_template_directory_uri(); ?>/img/menu/img4.png" alt="">
                                                </span>
                                            </div>
                                        </a>
                                    </li>   
                                </ul>
                            </li>
                            <li><a href="/kawa">Kawa</a></li>
                            <li><a href="/akcesoria">Akcesoria</a></li>
                            <li><a href="/o-lyreco">Lyreco</a></li>
                            <li><a href="/serwis">Serwis </a></li>
                            <li><a href="/kontakt">Kontakt</a></li>
                        </ul> 

                        <ul class="lang">
                            <li><a href="#">PL</a></li> 
                            <li><a href="#">EN</a></li>
                        </ul>
                        
                        <!-- SEARCH -->

                        <i class="fa fa-search search-open" aria-hidden="true"></i>
                        </div>
                        <!-- edn:SEARCH -->
           
            </div>
        </div>

        <aside id="main-search-filed">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                            <form role="search" method="get" id="searchform" class="searchform" action=" ">
                            <input type="text" value="" name="s" id="s" placeholder="Czego szukasz?"> 
                            <button type="submit" id="main-search-btn">
                                <span class="fa fa-search fa-lg"></span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </aside>