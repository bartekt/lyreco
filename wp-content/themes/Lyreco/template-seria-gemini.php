<?php /* Template Name: Seria Gemini */ get_header(); ?>
	<body class="gemini">
	<!-- PageTop
    ================================================== --> 
	<section class="sliderWraper">
		        <!-- Nav (DropDownNav)
        ================================================== --> 
            
        <?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
		
        <div class="staticCover">
        	<div class="container">
        	    <!-- Breadcrumbs --> 
				<div class="breadcrumbs">
					<ul>
						<li> <a href="index.html">Home</a> </li>
						<li> <a href="">Ekspres</a> </li>
						<li> <span>Gemini</span> </li>
					</ul>
				</div>
	        	<div class="staticCoverWrapper">
	        		<div class="row">
	        			<div class="col-md-8">
	        				<img src="<?php echo get_template_directory_uri(); ?>/img/assets/gemini.png">
	        			</div>
	        			<div class="col-md-4">
		        			<div class="desc pull-right">
		        				<div class="title"><h1>Gemini 2231</h1></div>
		        				<p>Gemini 220 od Nespresso przygotuje doskonałe przepisy kawowe na bazie mleka dla Twoich klientów i pracowników. Wyposażona 
w podwójny system ekstrakcyjny i funkcję spieniania mleka, ta maszyna to idealne rozwiązanie dla miłośników kawy latte. Gemini 220 przygotowuje doskonałe Cappuccino 
i Latte Macchiato. Po prostu wybierz z trzech dostępnych rozmiarów kaw: ristretto, espresso lub lungo, a maszyna zajmie się resztą. </p>
		        			</div> 
	        			</div>
	        		</div> 
	        	</div> 
        	</div> 
        </div> 
	</section>
	
	<!-- SubPageContent
    ================================================== -->
	
	<section id="subPageContent">
		<div class="container">
			<div id="contactLine">
				<div class="row">
					<div class="col-md-8 col-md-8 col-sm-12">
						<h4>KONTAKT</h4>
						<div class="pill-left phone"><span>Dla biur:</span><a href="tel:801307007">801 30 70 07</a></div>
						<div class="pull-right mail"><span>Dla restauracji, hoteli, kawiarni:</span><a href="mail:kawa@lyreco.com">kawa@lyreco.com</a></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="pull-right">
							<button class="btn btn-custom btn-dark">NAPISZ DO NAS</button>
						</div>
					</div>
				</div>
			</div> 
		</div>
		<div class="dotPattern"> 
			<div class="container"> 
				<h4>Charekterystyka</h4> 
				<div class="wrap">
					<ul>
						<li><i></i><span>Gorąca woda</span></li>
						<li><i></i><span>Automatyczna <br />kontrola objętościfiliżanki</span></li>
						<li><i></i><span>Tryb oszczędzania energii</span></li>
						<li><i></i><span>Ristretto espresso i lungo</span></li>
						<li><i></i><span>Przepisy z gorącym i zimnym mlekiem</span></li>
						<li><i></i><span>Przygotowanie mleka za jednym dotknięciem przycisku</span></li>
						<li><i></i><span>Funkcja przygotowania Cappuciino, Caffe Latte i Latte Macchiato</span></li>
						<li><i></i><span>Możliwość zaprogramowania cyfrowego wyświetlacza na kilka języków</span></li>
						<li><i></i><span>Bezpośrednie połączenie do źródła wody</span></li>
						<li><i></i><span>Możliwość zaprogramowania wielkośći filiżanki</span></li>
					</ul> 
				</div>
			</div>
		</div>

		<div class="container">
			<h4>Szczegóły</h4>
			<div class="wrap details"> 
                <div class="firstRow">
                    <div class="boxSmaller border-start">
                        <ul>
                            <li>Moc (w wattach) : 9000 Watt</li>
                            <li>Ciśnienie : 13-16 Bars</li>
                            <li>Waga : 100 Kilogramów</li>
                            <li>Wymiary (szer. x gł. x wys.) : 100x 62 x 63 cm</li>
                            <li>Liczba boilerów (element grzejny) : 6</li>
                            <li>Pojemność pojemnika na zużyte kapsułki : 250</li>
                            <li>Extraction Heads : 4</li>
                            <li>Alarm odkamieniania</li>
                            <li>Tryb energooszczędny</li>
                            <li>Gwarancja : 2 lata</li>
                        </ul>
                    </div>
                    <div class="boxSmaller">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/assets/details_gemini_img1.png" alt=""> 
                    </div>
                    <div class="boxBigger border-end">
                          <ul> 
                            <li>Pół-automatyczna funkcja czyszczenia</li>
                            <li>Alarm wymiany filtra wody</li>
                            <li>Przygotowanie przepisów za jednym 
                            dotknięciem przycisku (przepisy mleczne)</li>
                            <li>Elektroniczne powiadomienie o zapełnieniu 
                            pojemnika na zużyte kapsułki</li>
                            <li>Regulowana podstawka pod wysokie 
                            szklanki, np. Latte Macchiato</li>
                            <li>Wbudowany pojemnik (5L) na mleko 
                            schładzany do temperatury 4°C</li>
                            <li>Możliwość zaprogramowania maszyny 
                            w kilku językach</li>
                        </ul>
                    </div>
                </div> 
            <div>
                <div class="boxSmaller">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/assets/details_gemini_img2.png" alt="">
                </div>
                <div class="boxSmaller border-center middleBox">
                    <ul> 
                        <li>Maszyna podłączona do sieci</li>
                        <li>Bezpośrednie podłączenie do źródła wody</li>
                        <li>Manualne wkładanie i usuwanie kapsułki</li>
                        <li>Tryb chroniący przed awarią każdą z głowic</li>
                        <li>Pół-automatyczna funkcja czyszczenia</li>
                        <li>Cyfrowy wielojęzyczny wyświetlacz</li>
                    </ul>
                </div>
                <div class="boxBigger lastBox">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/assets/details_gemini_img3.png" alt="">
                </div>
            </div> 
			</div>
		</div>

		<div class="container">
			<div id="contactLine">
				<div class="row">
					<div class="col-md-8 col-md-8 col-sm-12">
						<h4>KONTAKT</h4>
						<div class="pill-left phone"><span>Dla biur:</span><a href="tel:801307007">801 30 70 07</a></div>
						<div class="pull-right mail"><span>Dla restauracji, hoteli, kawiarni:</span><a href="mail:kawa@lyreco.com">kawa@lyreco.com</a></div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="pull-right">
							<button class="btn btn-custom btn-dark">NAPISZ DO NAS</button>
						</div>
					</div>
				</div>
			</div> 
		</div>

	</section>
<?php get_footer(); ?>
