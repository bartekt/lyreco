<?php /* Template Name: Serwis */ get_header(); ?>
 <body class="about-lyreco">

    <!-- PageTop
    ================================================== --> 
    <section class="sliderWraper"> 
    
        <!-- Nav (DropDownNav)
        ================================================== --> 
            
 		<?php get_template_part( 'menu' ); ?>
        
        <!-- SubPageCover
        ================================================== --> 
        
        <div class="staticCover">
            <div class="container"> 
                <div class="staticCoverWrapper">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-3">
 							<h1 class="line">Serwis</h1> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>

    <section id="subPageContent">
        <div class="container">
         	<div class="row"> 
	        	<div class="col-md-12"> 
					<h2><span>Serwis od początku do końca dla Twojej firmy.</span></h2>
					<p>
						Aby zaspokoić indywidualne potrzeby biznesowe Twojej firmy, Nespresso oferuje pełen zakres usług dla biznesu.<br />
						Wiedza i doświadczenie przeszkolonych Specjalistów ds. Kawy Nespresso pozostają zawsze do Twojej dyspozycji.     
					</p>
	            </div> <!-- ITEM ROW END -->
	        </div>
            <div class="row"> 

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Wyspecjalizowane usługi<br > dla klientów</span></h3>
							<p>
								Nasi Specjaliści ds. Kawy są zawsze gotowi
								do świadczenia zindywidualizowanych
								porad dotyczących naszej kawy, 
								urządzeń, akcesoriów i usług.
							</p>
						</div>
					</a> 
				</div>

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Dostawa kapsułek<br > w 48 godzin</span></h3>
							<p>
								Zamów swoje kapsułki Nespresso
								telefonicznie lub przez Internet,
								a otrzymasz je w ciągu dwóch
								dni roboczych.
							</p>
						</div>
					</a> 
				</div>
				 

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Przyjazna<br > klientom infolinia</span></h3>
							<p>
								Specjaliści ds. Kawy Nespresso są 
								w zasięgu jednego telefonu. Służą 
								wszelką pomocą, bez względu na to, 
								czy potrzebujesz wskazówek dotyczących 
								korzystania ze swojego urządzenia, 
								czy pomocy w przypadku problemów 
								technicznych.
							</p>
						</div>
					</a> 
				</div>
				 

            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Pełna<br /> obsługa urządzeń</span></h3>
							<p>
								Nespresso może zaoferować pełny
								serwis na miejscu w celu zapewnienia
								możliwie najlepszej kawy przygotowanej
								przy użyciu maszyny Nespresso.
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Wsparcie techniczne</span></h3>
							<p>
								Nasi technicy mogą udzielić wsparcia
								technicznego zgodnie z Twoimi potrzebami,
								wraz z pełnym zakresem usług
								konserwacyjnych na miejscu.
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Wypożyczenie<br /> zastępczej maszyny <br />na czas naprawy</span></h3>
							<p>
								Nespresso zobowiązuje się 
								do zapewnienia urządzenia zastępczego, 
								aby utrzymać nieprzerwaną możliwość
								oferowania kawy najwyższej jakości, 
								gdy Twoje urządzenie jest w serwisie
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box">
							<h3><span>Gwarancja<br />serwisowa</span></h3>
							<p>
								Klientowi Nespresso przysługuje
								standardowa gwarancja dla urządzenia
								oraz trzymiesięczna gwarancja
								dla wszystkich prac serwisowych.
							</p>
						</div>
					</a> 
				</div>
				 
            	<div class="col-md-3 item"> 
					<a href=""> 
						<div class="box active">
							<h3><span>Wypożyczenie<br /> urządzenia<br /> na specjalneokazje</span></h3>
							<p>
								Z przyjemnością wypożyczymy 
								dodatkowe urządzenie na specjalne 
								okazje. Dni otwarte, seminaria, konferencje 
								– niezależnie od okazji, pomożemy 
								w zapewnieniu Twoim Klientom 
								lub partnerom handlowym wyjątkowych
								kaw Nespresso Grand Cru.
							</p>
						</div>
					</a> 
				</div>
				 

            </div>
        </div> <!-- CONTAINER END -->
    </section>
<?php get_footer(); ?>
